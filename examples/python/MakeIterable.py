#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = too-few-public-methods

# ---------------
# MakeIterable.py
# ---------------

def range_iterator (b, e) :
    while b != e :
        yield b
        b += 1

@make_iterable
def range_iterable (b, e) :
    while b != e :
        yield b
        b += 1

map_iterable = make_iterable(map)

def test1 () :
    x = range_iterator(2, 5)
    assert list(x) == [2, 3, 4]
    assert list(x) == []

def test2 () :
    x = range_iterable(2, 5)
    assert list(x) == [2, 3, 4]
    assert list(x) == [2, 3, 4]

def test3 () :
    x = map(lambda v : v ** 2, (2, 3, 4))
    assert list(x) == [4, 9, 16]
    assert list(x) == []

def test4 () :
    x = map_iterable(lambda v : v ** 2, (2, 3, 4))
    assert list(x) == [4, 9, 16]
    assert list(x) == [4, 9, 16]

def main () :
    print("MakeIterable")
    for n in range(4) :
        eval("test" + str(n + 1) + "()")
    print("Done.")

if __name__ == "__main__" : # pragma: no cover
    main()
