# -----------
# Wed, 10 Apr
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Tue, 6-8pm, GDC 1.302
    Thu, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
% in SQL (Like) is like .* in RE
_ in SQL (Like) is like .  in RE
"""
