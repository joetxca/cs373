# -----------
# Mon, 25 Mar
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
relational database
relational algebra
"""

"""
algebras
    elements
    operations
"""

"""
integer arithmetic
    integers
    + closed
    - closed
    * closed
    / open
"""

"""
algebras are sometimes open or closed on their operations
"""

"""
relational algebra
    relations (tables)
    select
    project
    join (several flavors)
closed on all operations
"""

"""
movie table

"name" "year" "director" "genre"
"shane" "1953" "george stevens" "western"
"star wars" "1977" "geor
"""

[
{"name": "star wars",
 "year": 1977,
 "director": "george lucas",
 "genre": "sci-fi"}
...
]

"""
select
    a relation (a table)
    a unary predicate (a unary function that returns a bool)
less than or equal to the number of rows coming in
same number of cols
winnowing the rows
"""

"""
project
    a relation (a table)
    a series of attributes (col names)
less than or equal to the number of cols coming in
same number of rows
winnowing the cols
"""

"""
Python is procedural
SQL    is declarative
"""
