# -----------
# Mon, 10 Apr
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
Movie table

title year director genre
"star wars" 1977 "george lucas" "sci-fi"
"shane" 1953 "george stevens" "western"
...
"""

"""
Director table (id is primary key)

id name
1 "george lucas"
2 "george stevens"
...

Movie table (director_id is foreign key)

title year director_id genre
"star wars" 1977 1 "sci-fi"
"shane" 1953 2 "western"
...
"""

"""
when you have these foreign key constraints

create tables
    director
    movie

drop tables
    movie
    director
"""
