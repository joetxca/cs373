 # -----------
# Wed, 13 Feb
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

class A :
    def __iter__ (self) :
        return A_Iterator()

class A_Iterator :
    def __next__ (self) :
        # return the next value
        # eventually raise a StopIteration exception

x = A()
print(type(x)) # A

p = iter(x)
print(type(p)) # A_Iterator

print(p is x)  # false

v = next(p)

"""
run f() on the first element of x
run g() on the rest of the elements x
""""

x = A()

for v in x : # not going to work
    f(v)     # will run f() on all elements of x

p = iter(x) # x.__iter__()
f(next(p))  # p.__next__()

try :
    while True
        g(next(p))     # p.__next__()
except StopIteration :
    pass

for v in p : # better and faster
    g(v)

a = [2, 3, 4]
print(type(a)) # list

p = iter(a)    # a.__iter__()
print(type(p)) # list iterator

print(p is a)  # false

q = iter(p)    # p.__iter__()
print(type(q)) # list iterator

print(q is p)  # true

class A :
    def __iter__ (self) :
        return A_Iterator()

class A_Iterator :
    def __next__ (self) :
        # return the next value
        # eventually raise a StopIteration exception

    def __iter__ (self) :
        return self

"""
reduce is the first example of a higher-order function
"""

"""
reduce takes:
    a binary function
    an iterable
    a value

reduce returns:
    v # a0 # a1 # a2 # a3 # ... # an-1
"""

"""
+
a seq of numbers
0
sum
"""

"""
*
a seq of numbers
1
product
"""

"""
*
1..n
1
factorial
"""
