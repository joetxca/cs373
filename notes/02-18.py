# -----------
# Mon, 18 Feb
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
1 # 2 # 3 # 4
  1   # 3 # 4
      2   # 4
          2
"""

"""
IMDB has at three models
    movies
    tv shows
    people
"""

"""
find a topic with at least three models
    some form civic engagement
    three disparate RESTful API data sources
"""

"""
huge disconnect between this 4-phase project and what we do in class
"""

"""
don't confuse the RESTful APIs that you are going to scrape to get data
with the RESTful that your site will provide
"""

"""
a frontend (Docker image)
a backend  (Docker image)
your frontend is going to consume that RESTful of the backend
"""

"""
the only programming in P1 is the GitLab API for user stats
"""

"""
don't confuse a model page (grid) with an instance page
"""
