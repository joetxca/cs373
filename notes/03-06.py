# -----------
# Wed,  6 Mar
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

# Java

Tiger  x = new Mammal(...); # no
Mammal x = new Tiger(...);

# Python

class square3 :
    def __call__ (self, v) :
        return v ** 2

print(square3(2)) # no

x = square3()
print(x(2))          # 4
print(x.__call__(2)) # 4

print(square3()(2))  # 4

"""
a closure is function that can refer to data
that is not an arg and not global
"""

"""
generators are closures that close on objects
lambdas    are closures that close on names
"""
