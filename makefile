.DEFAULT_GOAL := all

all:

clean:
	cd examples/python; make clean
	@echo
	cd examples/javascript; make clean

config:
	git config -l

init:
	touch README
	git init
	git remote add origin git@gitlab.com:gpdowning/cs373.git
	git add README
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add examples
	git add makefile
	git add notes
	git commit -m "another commit"
	git push
	git status

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	@rsync -r -t -u -v --delete            \
    --include "Script.txt"                 \
    --include "Dockerfile"                 \
    --include "Hello.py"                   \
    --include "Assertions.py"              \
    --include "UnitTests1.py"              \
    --include "UnitTests2.py"              \
    --include "UnitTests3.py"              \
    --include "Coverage1.py"               \
    --include "Coverage2.py"               \
    --include "Coverage3.py"               \
    --include "IsPrime.py"                 \
    --include "IsPrimeT.py"                \
    --include "Exceptions.py"              \
    --include "Types.py"                   \
    --include "Operators.py"               \
    --include "Slices.py"                  \
    --include "Factorial.py"               \
    --include "FactorialT.py"              \
    --include "Reduce.py"                  \
    --include "ReduceT.py"                 \
    --include "Yield.py"                   \
    --include "RangeIterator.py"           \
    --include "RangeIteratorT.py"          \
    --include "Range.py"                   \
    --include "RangeT.py"                  \
    --include "Iteration.py"               \
    --include "Comprehensions.py"          \
    --include "Map.py"                     \
    --include "MapT.py"                    \
    --include "Iterables.py"               \
    --include "Functions.py"               \
    --include "FunctionDefaults.py"        \
    --include "FunctionKeywords.py"        \
    --include "FunctionUnpacking.py"       \
    --include "FunctionTuple.py"           \
    --include "FunctionDict.py"            \
    --include "Decorators.py"              \
    --include "MakeIterable.py"            \
    --include "MakeIterableT.py"           \
    --include "Select.py"                  \
    --include "SelectT.py"                 \
    --include "Project.py"                 \
    --include "ProjectT.py"                \
    --include "CrossJoin.py"               \
    --include "CrossJoinT.py"              \
    --include "ThetaJoin.py"               \
    --include "ThetaJoinT.py"              \
    --include "NaturalJoin.py"             \
    --include "NaturalJoinT.py"            \
    --include "RegExps.py"                 \
    --exclude "*"                          \
    ../../examples/python/ examples/python/
	@rsync -r -t -u -v --delete            \
    --include "Script.txt"                 \
    --include "Dockerfile"                 \
    --include "Hello.js"                   \
    --include "Assertions.js"              \
    --include "UnitTests1.js"              \
    --include "UnitTests2.js"              \
    --include "UnitTests3.js"              \
    --include "Coverage1.js"               \
    --include "Coverage2.js"               \
    --include "Coverage3.js"               \
    --include "Exceptions.js"              \
    --include "Types.js"                   \
    --include "Operators.js"               \
    --include "Factorial.js"               \
    --include "FactorialT.js"              \
    --include "Reduce.js"                  \
    --include "ReduceT.js"                 \
    --include "Yield.js"                   \
    --include "RangeIterator.js"           \
    --include "RangeIteratorT.js"          \
    --include "Range.js"                   \
    --include "RangeT.js"                  \
    --include "Iteration.js"               \
    --include "Map.js"                     \
    --include "MapT.js"                    \
    --include "Functions.js"               \
    --include "FunctionDefaults.js"        \
    --include "FunctionUnpacking.js"       \
    --exclude "*"                          \
    ../../examples/javascript/ examples/javascript/
	@rsync -r -t -u -v --delete            \
    --include "ShowDatabases.sql"          \
    --include "ShowDatabases.out"          \
    --include "ShowEngines.sql"            \
    --include "ShowEngines.out"            \
    --include "CreateDatabase.sql"         \
    --include "CreateDatabase.out"         \
    --include "CreateTables.sql"           \
    --include "CreateTables.out"           \
    --include "Insert.sql"                 \
    --include "Insert.out"                 \
    --include "Select.sql"                 \
    --include "Select.out"                 \
    --include "Like.sql"                   \
    --include "Like.out"                   \
    --include "SelectT.sql"                \
    --include "SelectT.out"                \
    --include "Join.sql"                   \
    --include "Join.out"                   \
    --include "JoinT.sql"                  \
    --include "JoinT.out"                  \
    --exclude "*"                          \
    ../../examples/sql/ examples/sql
	@rsync -r -t -u -v --delete            \
    --include "StrategyPattern1.java"      \
    --include "StrategyPattern2.java"      \
    --exclude "*"                          \
    ../../examples/java/ examples/java/
	@rsync -r -t -u -v --delete            \
    --include "SAC.uml"                    \
    --include "SAC.svg"                    \
    --include "StrategyPattern1.uml"       \
    --include "StrategyPattern1.svg"       \
    --include "StrategyPattern2.uml"       \
    --include "StrategyPattern2.svg"       \
    --include "StrategyPattern3.uml"       \
    --include "StrategyPattern3.svg"       \
    --include "StrategyPattern4.uml"       \
    --include "StrategyPattern4.svg"       \
    --include "StrategyPattern5.uml"       \
    --include "StrategyPattern5.svg"       \
    --exclude "*"                          \
    ../../examples/uml/ examples/uml
	@rsync -r -t -u -v --delete            \
    --include "Collatz.py"                 \
    --include "RunCollatz.in"              \
    --include "RunCollatz.out"             \
    --include "RunCollatz.py"              \
    --include "Test.ctd"                   \
    --include "TestCollatz.py"             \
    --exclude "*"                          \
    ../../projects/python/collatz/ projects/collatz/
