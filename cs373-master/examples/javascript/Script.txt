% which docker
/usr/local/bin/docker



% docker --version
Docker version 18.09.1, build 4c52b90



% docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE



% cat Dockerfile
FROM node

RUN apt-get update          && \
    apt-get -y install vim

RUN npm update -g           && \
    npm --version           && \
    npm install    assert   && \
    npm install    lodash   && \
    npm install -g istanbul && \
    npm install -g jshint   && \
    npm install -g mocha    && \
    npm list

CMD bash



% docker build -t gpdowning/node .
...



% docker login
...



% docker push gpdowning/node
...



% docker pull gpdowning/node
...



% docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
gpdowning/node      latest              9621162fe510        5 seconds ago       974MB
node                latest              b064644cf368        11 days ago         673MB



% pwd
/Users/downing/cs373/git


% ls
README	_CS373.bbprojectd  examples  makefile  notes  projects



% docker run -it -v /Users/downing/cs373/git:/usr/node -w /usr/node gpdowning/node

root@9a51508a09c9:/usr/node# pwd
/usr/node



root@9a51508a09c9:/usr/node# ls
README	_CS373.bbprojectd  examples  makefile  notes  projects



root@9a51508a09c9:/usr/node# which git
/usr/bin/git
root@9a51508a09c9:/usr/node# git --version
git version 2.11.0



root@9a51508a09c9:/usr/node# which istanbul
/usr/local/bin/istanbul
root@9a51508a09c9:/usr/node# istanbul help
istanbul version:0.4.5



root@9a51508a09c9:/usr/node# which jshint
/usr/local/bin/jshint
root@9a51508a09c9:/usr/node# jshint --version
jshint v2.9.7



root@9a51508a09c9:/usr/node# which make
/usr/bin/make
root@9a51508a09c9:/usr/node# make --version
GNU Make 4.1



root@9a51508a09c9:/usr/node# which mocha
/usr/local/bin/mocha
root@9a51508a09c9:/usr/node# mocha --version
5.2.0



root@9a51508a09c9:/usr/node# which node
/usr/local/bin/node
root@9a51508a09c9:/usr/node# node --version
v11.7.0



root@9a51508a09c9:/usr/node# which npm
/usr/local/bin/npm
root@9a51508a09c9:/usr/node# npm --version
6.6.0



root@9a51508a09c9:/usr/node# which vim
/usr/bin/vim
root@9a51508a09c9:/usr/node# vim --version
VIM - Vi IMproved 8.0 (2016 Sep 12, compiled Sep 30 2017 18:21:38)
