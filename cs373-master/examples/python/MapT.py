#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement
# pylint: disable = too-few-public-methods

# --------
# MapT.py
# --------

# https://docs.python.org/3/library/functions.html#map

from timeit   import timeit
from typing   import Callable, Iterable, Iterator, TypeVar
from unittest import main, TestCase

T = TypeVar("T")

# 467 milliseconds
class map_iterator (Iterator[T]) :
    def __init__ (self, uf: Callable[[T], T], a: Iterable[T]) -> None :
        self.uf = uf
        self.p  = iter(a)

    def __iter__ (self) -> Iterator[T] :
        return self

    def __next__ (self) -> T :
        return self.uf(next(self.p))

# 318 milliseconds
def map_for (uf: Callable[[T], T], a: Iterable[T]) -> Iterator[T] :
    for v in a :
        yield uf(v)

# 320 milliseconds
def map_generator (uf: Callable[[T], T], a: Iterable[T]) -> Iterator[T] :
    return (uf(v) for v in a)

class MyUnitTests (TestCase) :
    def setUp (self) :
        self.a = [
            map_iterator,
            map_for,
            map_generator,
            map]

    def test1 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                m = f(lambda x : x ** 2, [2, 3, 4])
                assert     hasattr(m, "__next__")
                assert     hasattr(m, "__iter__")
                assert not hasattr(m, "__getitem__")
                assert not hasattr(m, "__len__")
                assert iter(m) is m
                self.assertEqual(list(m), [4, 9, 16])
                self.assertEqual(list(m), [])

    def test2 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                m = f(lambda x : x ** 3, (2, 3, 4))
                assert     hasattr(m, "__next__")
                assert     hasattr(m, "__iter__")
                assert not hasattr(m, "__getitem__")
                assert not hasattr(m, "__len__")
                assert iter(m) is m
                self.assertEqual(tuple(m), (8, 27, 64))
                self.assertEqual(tuple(m), ())

    def test3 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                m = f(lambda v : v ** 2, {2, 3, 4})
                assert     hasattr(m, "__next__")
                assert     hasattr(m, "__iter__")
                assert not hasattr(m, "__getitem__")
                assert not hasattr(m, "__len__")
                assert iter(m) is m
                self.assertEqual(set(m), {4, 9, 16})
                self.assertEqual(set(m), set())

    def test4 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                m = f(None, ())
                assert     hasattr(m, "__next__")
                assert     hasattr(m, "__iter__")
                assert not hasattr(m, "__getitem__")
                assert not hasattr(m, "__len__")
                assert iter(m) is m
                self.assertEqual(list(m), [])

    def test5 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                print()
                print(f.__name__)
                if f.__name__ == "map" :
                    t = timeit(
                        "list(" + f.__name__ + "(lambda x : x ** 2, 10000 * [5]))",
                        "",
                        number = 100)
                else :
                    t = timeit(
                        "list(" + f.__name__ + "(lambda x : x ** 2, 10000 * [5]))",
                        "from __main__ import " + f.__name__,
                        number = 100)
                print("{:.2f} milliseconds".format(t * 1000))

if __name__ == "__main__" : # pragma: no cover
    main()

""" #pragma: no cover
% MapT.py
..
map_iterator
466.68 milliseconds

map_for
317.70 milliseconds

map_generator
320.38 milliseconds

map
301.79 milliseconds
.
----------------------------------------------------------------------
Ran 3 tests in 2.061s

OK
"""
