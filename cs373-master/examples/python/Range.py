#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# --------
# Range.py
# --------

# https://docs.python.org/3/library/functions.html#func-range

def test1 () :
    x = range(2, 2)
    assert not hasattr(x, "__next__")
    assert     hasattr(x, "__iter__")
    assert     hasattr(x, "__getitem__")
    assert iter(x) is not x
    assert list(x) == []

def test2 () :
    x = range(2, 3)
    assert not hasattr(x, "__next__")
    assert     hasattr(x, "__iter__")
    assert     hasattr(x, "__getitem__")
    assert iter(x) is not x
    assert list(x) == [2]
    assert list(x) == [2]

def test3 () :
    x = range(2, 4)
    assert not hasattr(x, "__next__")
    assert     hasattr(x, "__iter__")
    assert     hasattr(x, "__getitem__")
    assert iter(x) is not x
    assert list(x) == [2, 3]
    assert list(x) == [2, 3]

def test4 () :
    x = range(2, 5)
    assert not hasattr(x, "__next__")
    assert     hasattr(x, "__iter__")
    assert     hasattr(x, "__getitem__")
    assert iter(x) is not x
    assert list(x) == [2, 3, 4]
    assert list(x) == [2, 3, 4]

def test5 () :
    x = range(2, 5)
    assert not hasattr(x, "__next__")
    assert     hasattr(x, "__iter__")
    assert     hasattr(x, "__getitem__")
    assert iter(x) is not x
    assert x[0]  == 2
    assert x[1]  == 3
    assert x[2]  == 4
    assert x[-1] == 4
    try :
        assert x[3] == 5
    except IndexError as e :
        assert e.args == ('range object index out of range',)

def main () :
    print("Range.py")
    for n in range(5) :
        eval("test" + str(n + 1) + "()")
    print("Done.")

if __name__ == "__main__" : # pragma: no cover
    main()
