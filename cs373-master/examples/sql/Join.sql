-- --------
-- Join.sql
-- --------

-- https://www.w3schools.com/sql/sql_join_inner.asp
-- https://www.w3schools.com/sql/sql_join.asp

use test;

-- -----------------------------------------------------------------------
select "*** show Student ***";
select * from Student;

-- -----------------------------------------------------------------------
select "*** show College ***";
select * from College;

-- -----------------------------------------------------------------------
select "*** show Apply ***";
select * from Apply;

-- -----------------------------------------------------------------------
-- relational algebra cross join
select "*** query #1 ***";
select *
    from Student
    cross join Apply
    order by Student.sID;

-- -----------------------------------------------------------------------
-- relational algebra theta join (with on)
-- notice duplicate sID attribute
select "*** query #2a ***";
select *
    from Student
    inner join Apply
    on Student.sID = Apply.sID;

-- -----------------------------------------------------------------------
-- relational algebra theta join (with using)
-- notice NO duplicate sID attribute
select "*** query #2b ***";
select *
    from Student
    inner join Apply using (sID);

-- -----------------------------------------------------------------------
-- relational algebra natural join
select "*** query #2c ***";
select *
    from Student
    natural join Apply;

-- -----------------------------------------------------------------------
-- student name and GPA of students who applied in "CS" but were not accepted
-- inner join
select "*** query #3a ***";
select sName, GPA
    from Student
    inner join Apply using (sID)
    where major = "CS" and not decision;

-- -----------------------------------------------------------------------
-- subquery (with in)
select "*** query #3b ***";
select sName, GPA
    from Student
    where sID in
        (select sID
            from Apply
            where major = "CS" and not decision);

-- -----------------------------------------------------------------------
-- student name and GPA of students who applied in "CS" to colleges with an enrollment greater than 20000
-- inner join
select "*** query #4 ***";
select sName, GPA
    from Student
        inner join Apply   using (sID)
        inner join College using (cName)
    where major = "CS" and enrollment > 20000;

-- -----------------------------------------------------------------------
-- name of college and state if there's another college in the same state
-- self join (using as)
select "*** query #5a ***";
select R.cName, R.state
    from College as R
    inner join College as S
    where R.cName != S.cName and R.state = S.state;

-- -----------------------------------------------------------------------
-- subquery (with exists)
select "*** query #5b ***";
select cName, state
    from College as R
    where exists
        (select *
            from College as S
            where R.cName != S.cName and R.state = S.state);

-- -----------------------------------------------------------------------
-- subquery (with group by and having)
select "*** query #5c ***";
select cName, state
    from College
    natural join
        (select State
            from College
            group by State
            having count(State) > 1) as T;

exit
