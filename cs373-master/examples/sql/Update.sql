-- ----------
-- Update.sql
-- ----------

use test;

-- -----------------------------------------------------------------------
select "*** drop table Apply ***";
drop table if exists Apply;

-- -----------------------------------------------------------------------
select "*** drop table College ***";
drop table if exists College;

-- -----------------------------------------------------------------------
select "*** drop table Student ***";
drop table if exists Student;

-- -----------------------------------------------------------------------
select "*** create table Student ***";
create table Student (
    sID         int not null,
    sName       text,
    GPA         float,
    sizeHS      int,
    primary key (sID));

-- -----------------------------------------------------------------------
select "*** create table College ***";
create table College (
    cName       varchar(8) not null,
    state       char(2),
    enrollment  int,
    primary key (cName));

-- -----------------------------------------------------------------------
select "*** create table Apply ***";
create table Apply (
    sID         int,
    cName       varchar(8),
    major       text,
    decision    boolean);

-- -----------------------------------------------------------------------
select "*** insert Student ***";
insert into Student values (123, "Amy",    3.9,  1000);
insert into Student values (234, "Bob",    3.6,  1500);
insert into Student values (320, "Lori",   null, 2500);
insert into Student values (321, "Mary",   2.5,  1200);
insert into Student values (345, "Craig",  3.5,   500);
insert into Student values (432, "Kevin",  null, 1500);
insert into Student values (456, "Doris",  3.9,  1000);
insert into Student values (543, "Craig",  3.4,  2000);
insert into Student values (567, "Edward", 2.9,  2000);
insert into Student values (654, "Amy",    3.9,  1000);
insert into Student values (678, "Fay",    3.8,   200);
insert into Student values (765, "Jay",    2.9,  1500);
insert into Student values (789, "Gary",   3.4,   800);
insert into Student values (876, "Irene",  3.9,   400);
insert into Student values (987, "Helen",  3.7,   800);

-- -----------------------------------------------------------------------
select "*** insert College ***";
insert into College values ("A&M",      "TX", 25000);
insert into College values ("Berkeley", "CA", 36000);
insert into College values ("Cornell",  "NY", 21000);
insert into College values ("MIT",      "MA", 10000);
insert into College values ("Stanford", "CA", 15000);
insert into College values ("UCF",      "FL", 36000);

-- -----------------------------------------------------------------------
select "*** insert Apply ***";
insert into Apply values (123, "Berkeley", "CS",             true);
insert into Apply values (123, "Cornell",  "EE",             true);
insert into Apply values (123, "Stanford", "CS",             true);
insert into Apply values (123, "Stanford", "EE",             false);
insert into Apply values (234, "Berkeley", "biology",        false);
insert into Apply values (321, "MIT",      "history",        false);
insert into Apply values (321, "MIT",      "psychology",     true);
insert into Apply values (345, "Cornell",  "bioengineering", false);
insert into Apply values (345, "Cornell",  "CS",             true);
insert into Apply values (345, "Cornell",  "EE",             false);
insert into Apply values (345, "MIT",      "bioengineering", true);
insert into Apply values (543, "MIT",      "CS",             false);
insert into Apply values (678, "Stanford", "history",        true);
insert into Apply values (765, "Cornell",  "history",        false);
insert into Apply values (765, "Cornell",  "psychology",     true);
insert into Apply values (765, "Stanford", "history",        true);
insert into Apply values (876, "MIT",      "biology",        true);
insert into Apply values (876, "MIT",      "marine biology", false);
insert into Apply values (876, "Stanford", "CS",             false);
insert into Apply values (987, "Berkeley", "CS",             true);
insert into Apply values (987, "Stanford", "CS",             true);

-- -----------------------------------------------------------------------
select "*** show Student ***";
select * from Student;

-- -----------------------------------------------------------------------
select "*** show College ***";
select * from College;

-- -----------------------------------------------------------------------
select "*** show Apply ***";
select * from Apply;

-- ------------------------------------------------------------------------
-- applications of students who applied to Cornell with a GPA < 3.6

select "*** query #1a ***";
select count(*)
    from Apply
    where
        (cName = 'Cornell')
        and
        sID in
            (select sID
                from Student
                where GPA < 3.6);

select *
    from Apply
    where
        (cName = 'Cornell')
        and
        sID in
            (select sID
                from Student
                where GPA < 3.6);

-- ------------------------------------------------------------------------
-- applications of students who applied to UT with a GPA < 3.6

select "*** query #1b ***";
select count(*)
    from Apply
    where
        (cName = 'UT')
        and
        sID in
            (select sID
                from Student
                where GPA < 3.6);

-- ------------------------------------------------------------------------
-- change the Cornell applications to UT applications and have them accepted
-- why is this dangerous?

select "*** query #1c ***";
update Apply
    set cName = 'UT', decision = true
    where
        (cName = 'Cornell')
        and
        sID in
            (select sID
                from Student
                where GPA < 3.6);

-- ------------------------------------------------------------------------
-- applications of students who applied to UT with a GPA < 3.6

select "*** query #1d ***";
select count(*)
    from Apply
    where
        (cName = 'UT')
        and
        sID in
            (select sID
                from Student
                where GPA < 3.6);

select *
    from Apply
    where
        (cName = 'UT')
        and
        sID in
            (select sID
                from Student
                where GPA < 3.6);

-- ------------------------------------------------------------------------
-- applications of students who applied to CS

select "*** query #2a ***";
select count(*)
    from Apply
    where major = 'CS';

select *
    from Student
    inner join Apply using (sID)
    where major = 'CS'
    order by GPA desc;

-- ------------------------------------------------------------------------
-- applications of students who applied to CS with the highest GPA

select "*** query #2b ***";
select *
    from Student
    inner join Apply using (sID)
    where
        (major = 'CS')
        and
        sID in
            (select sID
                from Student
                where GPA >= all
                    (select GPA
                        from Student
                        where sID in
                            (select sID
                                from Apply
                                where major = 'CS')));

-- ------------------------------------------------------------------------
-- applications of students who applied to CSE

select "*** query #2c ***";
select count(*)
    from Student
    inner join Apply using (sID)
    where major = 'CSE'
    order by GPA desc;

-- ------------------------------------------------------------------------
-- change those applications from CS to CSE

select "*** query #2d ***";
create temporary table T as
    (select sID
        from Student
        where GPA >= all
            (select GPA
                from Student
                where sID in
                    (select sID
                        from Apply
                        where major = 'CS')));

select "*** query #2e ***";
update Apply
    set major = 'CSE'
    where
        (major = 'CS')
        and
        sID in
            (select *
                from T);

-- ------------------------------------------------------------------------
-- applications of students who applied to CSE

select "*** query #2f ***";
select count(*)
    from Student
    inner join Apply using (sID)
    where major = 'CSE'
    order by GPA desc;

select *
    from Student
    inner join Apply using (sID)
    where major = 'CSE'
    order by GPA desc;

-- ------------------------------------------------------------------------
-- all students

select "*** query #3a ***";
select count(*) from Student;
select *        from Student;

-- ------------------------------------------------------------------------
-- change every student to have the highest GPA and smallest school size

select "*** query #3b ***";
create temporary table R as
    select max(GPA)
        from Student;

select "*** query #3c ***";
create temporary table S as
    select min(sizeHS)
        from Student;

select "*** query #3d ***";
update Student
    set
        GPA =
            (select *
                from R),
        sizeHS =
            (select *
                from S);

-- ------------------------------------------------------------------------
-- all students

select "*** query #3e ***";
select count(*) from Student;
select *        from Student;

-- ------------------------------------------------------------------------
-- all applications

select "*** query #4a ***";
select count(*) from Apply;
select *        from Apply;

-- ------------------------------------------------------------------------
-- accept all students

select "*** query #4b ***";
update Apply
    set decision = true;

-- ------------------------------------------------------------------------
-- all applications

select "*** query #4c ***";
select count(*) from Apply;
select *        from Apply;

-- ------------------------------------------------------------------------
drop table if exists Student;
drop table if exists Apply;
drop table if exists College;

exit
