// ----------------------
// SingletonPatternT.java
// ----------------------

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

class Singleton1 {
    private static final Singleton1 _only = new Singleton1();

    private Singleton1 ()
        {}

    public static Singleton1 only () {
        return _only;}

    public String f () {
        return "Singleton.f()";}}

class Singleton2 {
    private static Singleton2 _only;

    private Singleton2 ()
        {}

    public static Singleton2 only () {
        if (Singleton2._only == null)
            Singleton2._only = new Singleton2();
        return Singleton2._only;}

    public String f () {
        return "Singleton.f()";}}

class Singleton3 {
    private Singleton3 ()
        {}

    private static class Holder {
        private static final Singleton3 _only = new Singleton3();}

    public static Singleton3 only () {
        return Holder._only;}

    public String f () {
        return "Singleton.f()";}}

public final class SingletonPatternT extends TestCase {
    public void test1 () {
    	assertEquals(Singleton1.only(), Singleton1.only());
    	assertEquals(Singleton1.only().f(), "Singleton.f()");}

    public void test2 () {
    	assertEquals(Singleton2.only(), Singleton2.only());
    	assertEquals(Singleton2.only().f(), "Singleton.f()");}

    public void test3 () {
    	assertEquals(Singleton3.only(), Singleton3.only());
    	assertEquals(Singleton3.only().f(), "Singleton.f()");}

    public static void main (String[] args) {
        TestRunner.run(new TestSuite(SingletonPatternT.class));}}
