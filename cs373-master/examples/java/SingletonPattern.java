// ---------------------
// SingletonPattern.java
// ---------------------

class Singleton {
    public static Singleton only () {
        return ...;}

    public String f () {
        return "Singleton.f()";}}

public final class SingletonPattern {
    public static void test () {
//      Singleton x = new Singleton();                    // error
    	assert(Singleton.only()     == Singleton.only());
    	assert(Singleton.only().f() == "Singleton.f()");}

    public static void main (String[] args) {
        System.out.println("SingletonPattern.java");
        test();
        System.out.println("Done.");}}
