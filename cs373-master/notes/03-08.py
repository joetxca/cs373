# -----------
# Fri,  8 Mar
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
three tokens
    =, *, **
two contexts
    function argument lists
    function calls
"""

"""
= in a function definition is a default (optional) argument
default arguments must be the trailing arguments
don't use mutable default values
"""

"""
= in a function call
call by name instead of call by position
in call by name order doesn't matter
skip over default parameters
the calls become sensitive to the names
by name arguments must be the trailing arguments
"""

f(2, 3, 4)
f(velocity=2, temperature=3, height=4)

"""
a * in a function argument list
there can only one of these
can't be last
the arguments must be by name
"""

def timeit (python_code, python_import, *, number) :
    ...

"""
a * in a function call
requires an iterable
also useable in a list, tuple, set construction
"""

"""
a ** in a function call
has to come after by position
has to come after iterable unpacking
"""

"""
a * in a function definition
collects extra by-position and iterable-unpacking
it must either be last or next to last, if ** is last
"""

"""
a ** in a function definition
collects extra by-name and dict-unpacking
"""

def f (*t, **d) :
    ...
