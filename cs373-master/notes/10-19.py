# -----------
# Fri, 19 Oct
# -----------

"""
algebra
    elements
    operations
"""

"""
integer arithmetic
    elements: integers
    operations: +, -, *, /, %

some operations closed, some are open
+, -, *, %: closed
/: open
"""

"""
relational algebra
    elements: relations (tables, tuples)
    operations: select, project, join (many flavors)

all operations are closed
"""

"""
movie table

title year director genre
shane 1953 "george stevens" western
"star wars" 1977 "george lucas" western
"""

"""
select(relation, predicate)
select(movie, movies made after 1960)
select produces less than or equal as many rows as are in the relation
select produces as many cols as are in the relation
"""

[
{"title": "shane", "year": 1953, "director": "george stevens", "genre": "western"}
{...}
...
]

"""
write select() twice
with yield
without yield
"""

def select (r, f) :
    for d in r :
        if f(d) :
            yield d

def select (r, f) :
    return (d for d in r if f(d))

def select (r, f) :
    return filter(f, r)

"""
project(relation, attr1, attr2, ...)
project(movie, director, year)
project produces less than or equal as many cols as are in the relation
project produces as many rows as are in the relation
"""

"""
write project() twice
with yield
without yield
"""
