// -----------
// Mon, 26 Nov
// -----------

/*
consequences of an abstract class
1. can't instantiate
*/

/*
consequences of an abstract method
1. the class must be declared abstract
2. the derived class must define it or be declared abstract
3. prohibited definition in the abstract base class
*/

class A {
    public void f (int) {...}}

class B extends A {
    public void f (int) {...}}

class T {
    public static void main (...) {
        B x = new B;
        x.f(2);      // B.f

        A y;
        if (...)
            y = new A;
        else
            y = new B;
        y.f(2);        // <the right one>

class A {
    public Mammal f (long) {...}}

class B extends A {
    public Tiger f (int) {...}} // must keep the signature the same
                                // covariant return type, name, number of args, type of args

class T {
    public static void main (...) {
        B x = new B;
        x.f(2);      // B.f

        A y;
        if (...)
            y = new A;
        else
            y = new B;
        y.f(2);        // A.f

class A {
    void f (long) {...}       // bad idea, if A has derived classes, use abstract methods instead
    final void g (long) {...}
    abstract void h (long);}

/*
f -> derived classes can optionally define it
g -> derived classes can not define it
h -> derived classes must be define it or be declared abstract
*/
