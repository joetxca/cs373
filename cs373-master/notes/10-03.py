# -----------
# Wed,  3 Oct
# -----------

x = range(2, 5)
print(type(x))  # range

print(x[0])              # 2
print(x.__getitem__ (0)) # 2

print(x[-1])              # 4
print(x.__getitem__ (-1)) # 4

p = iter(x)
p = x.__iter__()

print(type(p))           # range_iterator
print(next(p))           # 2
print(p.__next__())      # 2

class range :
    def __init__ (self, b, e) :
        self.b = b
        self.e = e

    def __getitem__ (self, i) :
        if i >= 0 :
            return self.b + i
        return self.e + i

    def __next__ ( # no

    def range_iterator (b, e) :
        while b != e :
            yield b
            b += 1

    def __iter__ (self) :
        return range.range_iterator(self.b, self.e)
//        b = self.b
//        while b != self.e :
//            yield b
//            b += 1
