# -----------
# Mon, 15 Apr
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Tue, 6-8pm, GDC 1.302
    Thu, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
10am #1
Customer                 - Developer

This Is Congress Exposed - Living Dangerously - back left
My Future Education      - Volunteer for Me   - back right
The Charity Watch        - Green Car          - middle left or right
Vote Wisely              - Rainbow Connection - front left
Find a Dog for Me!       - Catastrophe World  - front right

left out: UnbEATable Food
"""

"""
10am #2
Customer                 - Developer

Living Dangerously       - My Future Education - back left
Volunteer for Me         - The Charity Watch   - back right
Green Car                - Vote Wisely         - middle left or right
Rainbow Connection       - Find a Dog for Me!  - front left
Catastrophe World        - UnbEATable Food     - front right

left out: This Is Congress Exposed
"""

"""
11am #1
Customer                 - Developer

Energenius               - Perfect Fit for Me       - back left
Go Explore!              - Politicians Taking Money - back right
Books and Bones          - Budget for Space         - middle left
Catch Me Outside         - Care for the Air         - middle right
Connect with Nature      - Earth First              - front left
In Need of Soup          - Global Antidote          - front right
"""

"""
11am #2
Customer                 - Developer

Perfect Fit for Me       - Go Explore!         - back left
Politicians Taking Money - Books and Bones     - back right
Budget for Space         - Catch me Outside    - middle left
Care for the Air         - Connect with Nature - middle right
Earth First              - In Need of Soup     - front left
Global Antidote          - Energenius          - front right
"""
