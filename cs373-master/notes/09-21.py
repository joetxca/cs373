# -----------
# Fri, 21 Sep
# -----------

# Java

class A {}

"""
do I get .equals()?
yes, defaults to Java's ==
"""

# Python

class A :
    pass

"""
do I get ==
yes defaults to Python's is
define __eq__() to override
"""

"""
Java has a closed object model
    all instances have the same data
    over time instances have the same data

Python has an open object model
    all instances do NOT necessarily have the same data
    over time instances do NOT necessarily have the same data
"""

"""
higher-order functions are functions that either take a
function as an argument or return a function as a result

example: sort
"""

"""
str's   index returns an r-value
list's  index returns an l-value
tuple's index returns an r-value

define __getitem__() to override
"""

"""
list's += is     tolerant on the right: must only be iterable
list's +  is NOT tolerant on the right: must be another list
"""

"""
list's += is NOT tolerant on the right: must be another tuple
list's +  is NOT tolerant on the right: must be another tuple
"""

x += y
# is that the same as
x = x + y

"""
NOT true for list,  mutables
    true for tuple, immutables
"""

"""
parallel assignment
"""

v1, v2, v3 = e1, e2, e3
v1, v2, v3 = an iterable of length 3
