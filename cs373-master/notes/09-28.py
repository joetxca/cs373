# -----------
# Fri, 28 Sep
# -----------

def reduce (bf, a, v) :
    for (i in a) :
        v = bf(v, i)
    return v

a = [2, 3, 4]

p = iter(a)
print(type(p)) # list iterator

q = iter(a)
print(type(q)) # list iterator

r = iter(p)
print(type(r)) # list iterator
print(r is p)  # true

a = [2, 3, 4]

p = iter(a)
f(next(p))

for v in p :
    g(v)
