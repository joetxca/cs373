# -----------
# Wed, 30 Jan
# -----------

"""
every time we do a quiz,      later that day I remove the code
every time we do an exercise, later that day I change the code to 1234

Piazza / Canvas
labs sessions, next one is tomorrow
Zoom office hours, 3:30pm on some days, I'll announce it on Piazza

exceptions
"""

# pretend that Python doesn't have exceptions

# 1. return a special value

def f (...) :
    ...
    if (<something wrong>)
        return <special value>
    ...

def g (...) :
    ...
    x = f(...)
    if (x == <special value>)
        <something wrong>

# 2. global variable

h = 0

def f (...) :
    global h
    ...
    if (<something wrong>)
        h = -1
        return ...
    ...

def g (...) :
    global h
    ...
    h = 0
    x = f(...)
    if (h == -1)
        <something wrong>

# 3. pass in a special variable, pass by reference, mutable

def f (..., e2) :
    ...
    if (<something wrong>)
        e2[0] = -1
        return ...
    ...

def g (...) :
    ...
    e = [0]
    x = f(..., e)
    if (e[0] == -1)
        <something wrong>

"""
assertions are not recoverable
three ways above are ignorable
exceptions are recoverable and not ignorable
"""

"""
1. raise and no try block,        go to higher block
2. raise and the right try block, handle exception
"""

def f (...) :
    ...
    if (<something wrong>)
        raise E(...)
    ...

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except E :
        <something wrong>
    else :
        # only runs when NO exception is raised
    finaly :
        # always runs
    ...

"""
3. throw and the wrong try block, go to higher block
"""

def f (...) :
    ...
    if (<something wrong>)
        raise Y(...)
    ...

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except E :
        <something wrong>
    else :
        # only runs when NO exception is raised
    finaly :
        # always runs
    ...

"""
4. child except clause must precede parent except clause
"""

def f (...) :
    ...
    if (<something wrong>)
        raise Tiger(...)
    ...

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except Tiger :
        <something wrong>
    except Mammal :
        <something wrong>
    else :
        # only runs when NO exception is raised
    finaly :
        # always runs
    ...

print(type(2))    # int
print(type(int))  # type
print(type(type)) # type

class NameError :
    # <data member> args which is a tuple

a = [2, 3, 4]
print(type(a)) # list, mutable

a = (2, 3, 4)
print(type(a)) # tuple, immutable

a = [2]
print(type(a)) # list

a = (2)
print(type(a)) # int

a = (2,)
print(type(a)) # tuple

"""
In Java
    what does ==        test? references
    what does .equals() test? content

In Python
    what does is test? references
    what does == test? content
"""

s = "abc"
t = "abc"
print(s is t) # false
print(s == t) # true
