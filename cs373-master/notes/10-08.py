# -----------
# Mon,  8 Oct
# -----------

s = {2, 3, 4}
t = {}        # dict

"""
three implementations
1. a class
2. a function with yield
3. a function without yield
"""

def map (uf, a) :
    for v in a :
        yield uf(v)

def map (uf, a) :
    return (uf(v) for v in a)

class map :
    def __init__ (self, uf, a) :
        self.uf = uf
        self.p  = iter(a)

    def __iter__ (self) :
        return self

    def __next__ (self) :
        return self.uf(next(self.p))
