# -----------
# Mon,  4 Mar
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
filter is another example of a higher-order function
filter args
    unary predicate
    iterable
"""

"""
[code] -> eager list
(code) -> lazy  generator
{code} -> eager set or dict
"""

"""
define map THREE ways
1. a function with    yield
2. a function without yield
3. a class
"""
