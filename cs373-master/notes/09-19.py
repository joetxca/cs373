# ------------
# Wed, 19 Sep
# ------------

"""
Lecture by Hannah Peeler
"""

"""
AWS - Amazon Web Services
"""

"""
Free Tier with limitations:
https://aws.amazon.com/free/
$150 free credit with Github Student Pack
"""

"""
Who uses it:
	Netflix
	Facebook
	Twitter
	Twitch
	ESPN
	etc.
"""

"""
Useful tools:
	RDS - Relational Database Service
	DynamoDB - NoSQL equivalent
	EC2 - Elastic Compute Cloud, web server
	API Gateway - API generation
	Elastic Beanstalk - analogous App Engine, web application
	Lambda - Runs your code in response to events, manages compute resources.
	Machine Learning tools:
		Rekognition - image processing
		Comprehend - Natural Language Processing
		Translate - translation
"""

"""
Tips and Tricks:
- Always make sure your choices are available in free tier
- Creation of database automatically created EC2 instance to match it
- Create database before creating application through Elastic Beanstalk
- Do not forget your master login and password
- Specialize security groups for your uses, keep security in mind
- Stop BOTH your RDS and EC2 instances when no one will be using them
    -- for EC2, stopping can lose temporary data so be careful
- Be mindful of data backups and snapshots
- Do not forget your master login and password
"""
