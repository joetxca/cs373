# -----------
# Mon, 22 Oct
# -----------

"""
Please vote!!!

Flawn Academic Center (FAC)
Perry-Casteñada Library (PCL)
M-Sa:  7am-7pm
Su:   12pm-6pm
"""

"""
cross_join(relation1, relation2)
produces as many rows as the product of the number of rows in relation1 and relation2
"""

"""
student table
sid, name, gpa, high school size

college table
name, state, enrollment

apply table
sid, college name, major, decision
"""

"""
cross_join(student table, apply table)
"""

"""
implement cross_join twice
with yield
without yield
"""

def cross_join (r, s) :
    for v in r :
        for w in s :
            yield {**v, **w}

def cross_join (r, s) :
    return ({**v, **w} for v in r for w in s)

"""
theta_join(relation1, relation2, binary predicate)
with a false predicate -> <nothing>
with a true  predicate -> cross_join
produces less than or equal to as many rows as the product of the number of rows in relation1 and relation2
theta_join: cross_join with select
"""

def theta_join (r, s, bp) :
    for v in r :
        for w in s :
            if bp(v, w)
                yield {**v, **w}

def theta_join (r, s, bp) :
    return ({**v, **w} for v in r for w in s if bp(v, w))

def theta_join (r, s, bp)        # wouldn't want to implement it this
    select(cross_join(r, s), bp)

"""
natural_join(relation1, relation2)
no matching attributes -> cross_join
matching attributes, but no matching values -> <nothing>
produces less than or equal to as many rows as the product of the number of rows in relation1 and relation2
natural_join: theta_join with built-in predicate
"""

def natural_join (r, s) :
    def bp (u, v) :
        ???
    for v in r :
        for w in s :
            if bp(v, w)
                yield {**v, **w}

def natural_join (r, s) :
    def bp (u, v) :
        ???
    return theta_join(r, s, bp)
