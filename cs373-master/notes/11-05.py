# -----------
# Mon,  5 Nov
# -----------

"""
when querying aggregates, you can then no longer query non-aggregates, with the exception of a group by

when doing group by the having clause must be related to the groups
"""
