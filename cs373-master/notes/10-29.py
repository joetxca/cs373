# -----------
# Mon, 29 Oct
# -----------

"""
Please vote!!!

Flawn Academic Center (FAC)
Perry-Casteñada Library (PCL)
M-Sa:  7am-7pm
Su:   12pm-6pm
"""

"""
movie table

title year director genre
"shane" 1953 "george stevens" "western"
"star wars" 1977 "george lucas" "sci-fi"
"""

"""
director table

id name
1 "george stevens"
2 "george lucas"

movie table

title year director_id genre
"shane" 1953 1 "western"
"star wars" 1977 2 "sci-fi"
"""
