# -----------
# Wed, 10 Oct
# -----------

"""
ACM competitive programming workshop
Wed, 10 Oct, 6pm, GDC 4.302
"""

print(type(list)) # type

x = [2, 3, 4]
print(type(x)) # list

class square3 :
    def __call__ (self, v) :
        return v ** 2

print(type(square3)) # type

x = square3()
print(type(x)) # square3

print(x(2))          # 4
print(x.__call__(2)) # 4

def test3 () :
    a = [2, 3, 4]
    m = map(square3,   a)        # no
    m = map(square3(), a)
    assert list(m) == [4, 9, 16]
    assert list(m) == []

print(range(3)) # <address>
print(list(range(3)) # [0, 1, 2]

"""
=, *, **
function definition: =, *(2), **
function call: =, *, **
"""

def f (x, y=2, z=3) :
    return [x, y, z]

print(f(1, 2))

def timeit (s1, s2, *, number)
