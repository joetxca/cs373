# -----------
# Wed, 28 Nov
# -----------

class A :
    pass

print(type(A))         # type

x = A()
y = A()

print(type(x))         # A

print(x is y)          # False

def f (c) :
    z = c()
    return lambda : z

B = f(A)
print(type(B))         # function

x = B()
print(type(x))         # A

y = B()
print(type(y))         # A

print(x is y)          # True

A = B()
print(type(A))         # <class '__main__.A'>

class A :
    pass

print(A() is A())      # False

def f (c) :
    z = c()
    return lambda : z

A = f(A)
print(type(A))         # function

print(A() is A())      # True

def f (c) :
    z = c()
    return lambda : z

@f
class A :
    pass

print(A() is A())      # True

print(type(A))         # function
print(type(A()))       # <class '__main__.A'>

x = type(A())()
print(type(x))         # <class '__main__.A'>

y = type(A())()
print(type(y))         # <class '__main__.A'>

print(x is y)          # False
