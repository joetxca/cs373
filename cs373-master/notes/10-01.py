# -----------
# Mon,  1 Oct
# -----------

x = range(2, 5)
print(type(x))  # range
print(x)        # <address>, no def of __str__
print(list(x))  # [2, 3, 4]
print(list(x))  # [2, 3, 4]

p = iter(x)
print(p)        # <address>, no def of __str__
print(type(p))  # range iterator
q = iter(p)
print(q is p)   # true
print(list(p))  # [2, 3, 4]
print(list(p))  # []

x = foo(2, 3)

p = iter(x) # x.__iter__()

class range_iterator :
    def __init__ (self, start, end) :
        self.start = start
        self.end   = end

    def __iter__ (self) :
        return self

    def __next __ (self) :
        if (self.start == self.end)
            raise StopIteration
        x = self.start
        self.start += 1
        return x

def f () :
    print("abc")
    yield 2
    print("def")
    yield 3
    print("ghi")

x = f()        # f does NOT run, magic runs, you get an iterator, a generator
print(type(x)) # <generator>
print(x)       # <address>

y = iter(x)
print(x is y)  # true

v = next(x)    # abc
print(v)       # 2

v = next(x)    # def
print(v)       # 3

v = next(x)    # ghi, raise StopIteration

def range_iterator (start, end) :
    while (start != end) :
        yield start
        start += 1
