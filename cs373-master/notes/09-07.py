# -----------
# Fri,	7 Sep
# -----------

"""
(3n + 1) / 2
3n/2 + 1/2
n + n/2 + 1/2
n + n/2 + 1
n + (n >> 1) + 1
"""

"""
unit testing is called white box testing
you can see the internals of the solution and test them

acceptance testing is called black box testing
you can't see the internals
you can only see the external behavior of the program
"""

"""
write code that is easy to run and easy to test

Collatz.py
	kernel
	the functions that really solve the problem
	oblivious about where input is coming from and where output is going to

RunCollatz.py
	import Collatz
	run harness

TestCollatz.py
	import Collatz
	test harness
	unittests
"""

"""
clone the code repo
run RunCollatz,  all tests succeed
run TestCollatz, all tests succeed
fix the tests
run the tests, all tests fail
fix the code
run the tests, all tests succeed
HackerRank
generate more tests
optimize the code
run the tests, all tests succeed
HackerRank
repeat...
"""

"""
REPL
read eval print loop
"""
