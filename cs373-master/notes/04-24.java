// -----------
// Wed, 24 Apr
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Tue, 6-8pm, GDC 1.302
    Thu, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
*/

/*
subclasses design pattern
strategy/state design pattern
*/

abstract class A {
    abstract void f ();}

class B {
    void g () {...}}

/*
what are the consequences of an abstract method?
    1. a derived class must define it or become abstract
    2. the base class must be declared abstract
    3. prohibited
*/

interface I {
    void f ();}

/*
all methods are abstract
*/

/*
extends vs implements

a class can extend another class (only one)
a class can extend an abstract class (only one)
an interface can extend another interface (many)

a class can implement an interface (every method)
an abstract class can implement an interface (some methods)
*/

abstract class A {
    void f () {...}
    final void g () {...}
    abstract void h ();}

/*
what is class A communicating to its children about
    f: optional
    g: prohibited
    h: required or become abstract
*/

class A {
    void f (int) {...}}

class B extends A {
    void f (int) {...}}

class Test {
    public static void main (...) {
        A x = new B;
        x.f(2);      // B.f(int)
        }}

abstract class A {
    void f (int) {...}       // don't do this in parents
    final void g (int) {...}
    abstract void h (long);}

class B extends A {
    void f (int) {...}
    void h (int) {...}}

class A1 {
    void f (int) {...}} // not marked abstract, so child not required to define
                        // also, change in either signature results in different runtime behavior
class B1 extends A1 {
    void f (int) {...}}

abstract class A2 {
    abstract void f (int); // can't define, so have to define helper
    void f_helper (int);
class B2 extends A2 {
    void f (int) {...}}

interface I {
    void f (int);}               // best of both worlds
abstract class A3 implements I { // default definition
    void f (int) {...}}          // change in any signature results in a compile-time error
class B3 extends A3 {
    void f (int) {...}}
