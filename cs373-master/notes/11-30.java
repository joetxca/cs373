# -----------
# Fri, 30 Nov
# -----------

/*
Java's reflection model
*/

class A {} // a class Class instance is created that describes class A

class Class {
    public static Class forName (String name) {
        ...}

    public Object newInstance () {
        ..}

class Test {
    public static void main (…) {
        x = A();
        y = A();

        // first way
        Class c1 = x.getClass();    // non-static method, class Object method
        Class c2 = y.getClass();
        System.out.print(c1 == c2); // true

        // second way
        Class c3 = A.class;         // static data, class Object data
        Class c4 = A.class;
        System.out.print(c3 == c4); // true
        System.out.print(c3 == c2); // true

        // third way
        Class c5 = Class.forName("A"); // static method, class Class method
        Class c6 = Class.forName("A");
        // what if A is not a class in my program
        System.out.print(c5 == c6); // true
        System.out.print(c5 == c2); // true

        Object x = c4.newInstance();   // creating a new instance of A
                                       // non-static method, class Class method
        // what if A has a private default constructor
        // what if A doesn't have a default constructor
        // what if A is an abstract class
        // what if A is an iterface
        A y = (A)x;
        }}

abstrat class A {
    public void f (int) {}
    public final void g (long) {}
    public abstract void h (long);}
